import React from 'react';
import PropTypes from 'prop-types';
import clsx from "clsx";
import './Answer.css';

const Answer = ({ answer, isSelected, toggleAnswer }) => {
  let classList = clsx('answer', isSelected && 'answer_selected');
  const handleClick = () => {
    toggleAnswer(answer.id);
  };

  return (
    <div className={classList} onClick={handleClick}>{answer.text}</div>
  );
};

Answer.propTypes = {
  answer: PropTypes.shape({
    id: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired
  }).isRequired,
  isSelected: PropTypes.bool.isRequired,
  toggleAnswer: PropTypes.func.isRequired,
};

export default Answer;
