import React from 'react';
import PropTypes from 'prop-types';
import './NavLink.css';

const NavLink = ({ text, href }) => {
  return (
    <a className="nav-link" href={href}>{text}</a>
  );
};

NavLink.propTypes = {
  text: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired
};

export default NavLink;
