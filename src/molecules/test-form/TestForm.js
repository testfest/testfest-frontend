import React, { Component } from 'react';
import PropTypes from 'prop-types'

import TextInput from "../../atoms/text-input/TextInput";
import Button from "../../atoms/button/Button";
import MultiSelect from "../../atoms/multiselect/MultiSelect";

import './TestForm.css';


class TestForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      specialties: [],
      totalScore: 0,
      passingScore: 0,
      time: 0
    };
    this.setStateProperty = this.setStateProperty.bind(this);
    this.setSpecialties = this.setSpecialties.bind(this);
    this.addTest = this.addTest.bind(this);
  }

  setStateProperty(event) {
    const { target } = event;
    const propertyName = target.name;
    const value = target.value;
    this.setState({
      [propertyName]: value
    })
  }

  addTest(event) {
    const { title, specialties, totalScore, passingScore, time } = this.state;
    const { onSubmit } = this.props;

    event.preventDefault();
    const isStateCorrect = () => {
      for (let property in this.state) {
        const value = this.state[property];
        if (value === '' || value === 0 || value.length === 0) {
          alert('Заполните все поля');
          return false;
        }
      }

      if (passingScore > totalScore) {
        alert('Проходной балл не должен превышать общее количество баллов');
        return false
      }

      return true;
    };

    if (!isStateCorrect()) {
      return;
    }

    const body = {
      testName: title,
      coursesIds: specialties,
      points: totalScore,
      minPoints: passingScore,
      timeMinutes: time
    };
    onSubmit(body)
  }

  setSpecialties(event) {
    const { options } = event.target;
    const newSpecialties = [];
    for (let option of options) {
      if (option.selected) {
        newSpecialties.push(option.value);
      }
    }

    this.setState({
      specialties: newSpecialties
    });
  }

  render() {
    const { courses } = this.props;
    const mapCourses = (course => {
      return {
        key: course.id,
        value: course.courseName
      }
    });

    return (
      <form className="test-form">
        <div className="test-form__fields-container">
          <div className="test-form__column">
            <TextInput
              handleChange={this.setStateProperty}
              type="text"
              name="title"
              classNames="text-input_size_full-width"
              placeholder="Название теста"/>
            <label>
              <p className="test-form__label test-form__label_with-padding">Специализации</p>
              <MultiSelect
                classNames="multiselect_size_full-width"
                name="specialties"
                options={courses.map(mapCourses)}
                onChange={this.setSpecialties}/>
            </label>
          </div>
          <div className="test-form__column">
            <label className="test-form__number-input">
              <p className="test-form__label">Количество баллов</p>
              <TextInput
                handleChange={this.setStateProperty}
                type="number"
                name="totalScore"
                classNames="text-input_size_small"/>
            </label>
            <label className="test-form__number-input">
              <p className="test-form__label">Проходной балл</p>
              <TextInput
                handleChange={this.setStateProperty}
                type="number"
                name="passingScore"
                classNames="text-input_size_small"/>
            </label>
            <label className="test-form__number-input">
              <p className="test-form__label">Время в минутах</p>
              <TextInput
                handleChange={this.setStateProperty}
                type="number"
                name="time"
                classNames="text-input_size_small"/>
            </label>
          </div>
        </div>
        <Button handleClick={this.addTest} classNames="button_size_middle button_center">Создать вопросы</Button>
      </form>
    )
  }
}

TestForm.propTypes = {
  courses: PropTypes.array,
  onSubmit: PropTypes.func
};

TestForm.defaultProps = {
  courses: [],
  onSubmit: () => {}
};

export default TestForm;
