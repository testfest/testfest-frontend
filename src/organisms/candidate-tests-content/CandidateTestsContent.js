import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";

import { getTests, getCertificate } from "./actions";
import TableHeader from "../../atoms/table-header/TableHeader";
import Entity from "../../molecules/entity/Entity";
import Button from "../../atoms/button/Button";

import './CandidateTestsContent.css';


class CandidateTestsContent extends Component {
  componentDidMount() {
    const { courseId, getTests } = this.props;
    getTests(courseId);
  }

  downloadCertificate() {
    const { getCertificate, courseId } = this.props;
    getCertificate(courseId);
  }

  render() {
    const { tests, course } = this.props;
    const { id, courseName, courseDescription } = course;
    const headers = ['Название теста', 'Баллы', 'Проходной балл', 'Кол-во вопросов', 'Время в минутах'];

    const createTestElement = test => {
      const { testId, testName, points, maxPoints, minPoints, questionsCount, timeMinutes } = test;
      let testItems = [];
      testItems.push(
        <a className="entity__link" key={testId} href={`/${id}/tests/${testId}`}>{testName}</a>
      );
      testItems.push(`${points}/${maxPoints}`);
      testItems.push(minPoints);
      testItems.push(questionsCount);
      testItems.push(timeMinutes);
      return <Entity key={testId} entityItems={testItems} headers={headers}/>
    };
    const testElements = tests.map(createTestElement);

    const isCertificateEnabled = () => {
      for (let test of tests) {
        const { points, minPoints } = test;
        if (points < minPoints) {
          return false;
        }
      }
      return true;
    };
    let certificateElement;
    if (isCertificateEnabled()) {
      certificateElement = <div className="candidate-tests-content__button-container">
        <Button handleClick={this.downloadCertificate.bind(this)}>Получить сертификат</Button>
      </div>
    }

    return (
      <div className="candidate-tests-content">
        <div className="candidate-tests-content__header">
          <div>
            <p className="candidate-tests-content__heading">Моя специализация</p>
            <h1 className="candidate-tests-content__specialty-name">{courseName}</h1>
            <p className="candidate-tests-content__specialty-description">{courseDescription}</p>
          </div>
          {certificateElement}
        </div>
        <div className="candidate-tests-list">
          <TableHeader headers={headers}/>
          {testElements}
        </div>
      </div>
    )
  }
}

CandidateTestsContent.propTypes = {
  courseId: PropTypes.string.isRequired
};

const mapStateToProps = (state) => ({
  tests: state.testsReducer.tests,
  course: state.testsReducer.course
});

const mapDispatchToProps = {
  getTests,
  getCertificate
};

export default connect(mapStateToProps, mapDispatchToProps)(CandidateTestsContent);
