import React, { Fragment, Component } from 'react';
import { withRouter } from "react-router-dom";

import Header from "../../molecules/header/Header";
import Menu from "../../molecules/menu/Menu";
import AdminQuestionContent from "../../organisms/admin-question-content/AdminQuestionContent";

class AdminQuestionPage extends Component {
  render() {
    const { match } = this.props;
    const { test } = match.params;

    return (
      <Fragment>
        <Header>
          <Menu/>
        </Header>
        <AdminQuestionContent testId={test}/>
      </Fragment>
    );
  }
}

export default withRouter(AdminQuestionPage);
