import React, { Fragment } from 'react';

import SpecialtyChoiceContent from "../../organisms/specialty-choice-content/SpecialtyChoiceContent";
import UserHeader from "../../organisms/user-header/UserHeader";


const SpecialtyChoicePage = () => {
  return (
    <Fragment>
      <UserHeader/>
      <SpecialtyChoiceContent/>
    </Fragment>
  );
};

export default SpecialtyChoicePage;
