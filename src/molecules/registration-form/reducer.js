import { types } from "./actions";
import { initInterceptor } from "../../utils/tokenConfig";

const initState = {
  success: false,
  error: false,
  pending: false
};

export default (state = initState, action) => {
  switch (action.type) {
    case types.REGISTER_PENDING: {
      return {
        ...state,
        success: false,
        pending: true,
        error: false
      };
    }
    case types.REGISTER_SUCCESS: {
      const { token } = action;
      localStorage.setItem('token', token);
      initInterceptor();

      return {
        ...state,
        success: true,
        pending: false,
        error: false
      }
    }
    case types.REGISTER_ERROR: {
      return {
        ...state,
        success: false,
        pending: false,
        error: true
      }
    }
    case types.REGISTER_FLUSH: {
      return {
        ...state,
        success: false,
        pending: false,
        error: false
      }
    }
    default: {
      return state;
    }
  }
}