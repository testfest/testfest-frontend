import React from 'react';
import PropTypes from 'prop-types';
import './Select.css';

const Select = ({ options, selectedOption, name, onChange }) => {
  const createOption = option => {
    return <option
      key={option}
      className="select__option"
      value={option}>
      {option}
    </option>
  };
  const optionElements = options.map(createOption);

  return (
    <select
      className="select"
      name={name}
      value={selectedOption}
      onChange={onChange}>
      {optionElements}
    </select>
  )
};

Select.propTypes = {
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  selectedOption: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

export default Select;
