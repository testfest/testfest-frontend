export const constants = {
  JWT_KEY: 'colbys@testfest.ru',
  FIRST_NAME_KEY: 'fname',
  LAST_NAME_KEY: 'lname',
  USER_ROLE_KEY: 'roles'
};

export const roles = {
  ROLE_ADMIN: 'ROLE_ADMIN',
  ROLE_USER: 'ROLE_USER'
};