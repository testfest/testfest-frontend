import { types } from "./actions";

const initState = {
  pending: false,
  successGet: false,
  successPost: false,
  error: false,
  tests: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case types.GET_TESTS_PENDING: {
      return {
        ...state,
        pending: true,
        successGet: false,
        error: false
      }
    }
    case types.GET_TESTS_SUCCESS: {
      return {
        ...state,
        pending: false,
        successGet: true,
        error: false,
        tests: action.tests
      }
    }
    case types.GET_TESTS_ERROR: {
      return {
        ...state,
        pending: false,
        successGet: false,
        error: true
      }
    }
    case types.CREATE_TEST_PENDING: {
      return {
        ...state,
        pending: true,
        successPost: false,
        error: false
      }
    }
    case types.CREATE_TEST_SUCCESS: {
      return {
        ...state,
        pending: false,
        successPost: true,
        error: false,
        tests: [...state.tests, action.test]
      }
    }
    case types.CREATE_TEST_ERROR: {
      return {
        ...state,
        pending: false,
        successPost: false,
        error: true
      }
    }
    case types.GET_TESTS_FLUSH: {
      return {
        ...state,
        pending: false,
        successGet: false,
        successPost: false,
        error: false,
        tests: []
      }
    }
    default: {
      return state;
    }
  }
}