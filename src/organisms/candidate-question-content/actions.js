import { get, post, to } from "../../utils/apiServiceBase";
import history from "../../utils/history";

export const types = {
  GET_QUESTIONS_PENDING: 'GET_QUESTIONS_PENDING',
  GET_QUESTIONS_SUCCESS: 'GET_QUESTIONS_SUCCESS',
  GET_QUESTIONS_ERROR: 'GET_QUESTIONS_ERROR',
  GET_QUESTIONS_FLUSH: 'GET_QUESTIONS_FLUSH',
  SUBMIT_RESULTS_PENDING: 'SUBMIT_RESULTS_PENDING',
  SUBMIT_RESULTS_SUCCESS: 'SUBMIT_RESULTS_SUCCESS',
  SUBMIT_RESULTS_ERROR: 'SUBMIT_RESULTS_ERROR'
};

export const getQuestions = (testId) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_QUESTIONS_PENDING
    });

    const testUrl = `/api/tests/${testId}`;
    const questionsUrl = `/api/questions?testId=${testId}`;

    const [tError, tResult] = await to(get(testUrl));
    const [qError, qResult] = await to(get(questionsUrl));

    if (qError || tError) {
      return dispatch({
        type: types.GET_QUESTIONS_ERROR
      })
    }

    return dispatch({
      type: types.GET_QUESTIONS_SUCCESS,
      test: tResult.data,
      questions: qResult.data
    })
  }
};

export const submitResults = (testId, userAnswers, courseId) => {
  return async (dispatch) => {
    dispatch({
      type: types.SUBMIT_RESULTS_PENDING
    });

    const filteredIds = userAnswers.map(a => a.questionId).filter((v, i, a) => a.indexOf(v) === i);
    const results = filteredIds.map(id => {
      const answers = userAnswers.filter(a => a.questionId === id).map(a => a.answerId);

      return {
        questionId: id,
        answers
      }
    });

    const resultsUrl = `/api/questions/results`;
    const body = {
      testId: testId,
      results: results
    };

    const [error] = await to(post(resultsUrl, body));
    if (error) {
      return dispatch({
        type: types.SUBMIT_RESULTS_ERROR
      });
    }

    dispatch({
      type: types.SUBMIT_RESULTS_SUCCESS
    });
    history.push(`/${courseId}/tests`);
  }
};

export const reset = () => {
  return (dispatch) => {
    dispatch({
      type: types.GET_QUESTIONS_FLUSH
    })
  }
};

