import { get, to } from "../../utils/apiServiceBase";


export const types = {
  CANDIDATE_TESTS_PENDING: 'CANDIDATE_TESTS_PENDING',
  CANDIDATE_TESTS_SUCCESS: 'CANDIDATE_TESTS_SUCCESS',
  CANDIDATE_TESTS_ERROR: 'CANDIDATE_TESTS_ERROR',
  CANDIDATE_TEST_FLUSH: 'CANDIDATE_TESTS_FLUSH',
  CERTIFICATE_PENDING: 'CERTIFICATE_PENDING',
  CERTIFICATE_SUCCESS_LOAD: 'CERTIFICATE_SUCCESS_LOAD',
  CERTIFICATE_SUCCESS_NO_LOAD: 'CERTIFICATE_SUCCESS_NO_LOAD',
  CERTIFICATE_ERROR: 'CERTIFICATE_ERROR'
};

export const getTests = (courseId) => {
  return async (dispatch) => {
    dispatch({
      type: types.CANDIDATE_TESTS_PENDING
    });

    const courseUrl = `/api/courses/${courseId}`;
    const testsUrl = `/api/tests?courseId=${courseId}`;

    const [cError, cResult] = await to(get(courseUrl));
    const [tError, tResult] = await to(get(testsUrl));

    if (cError || tError) {
      return dispatch({
        type: types.CANDIDATE_TESTS_ERROR
      })
    }

    return dispatch({
      type: types.CANDIDATE_TESTS_SUCCESS,
      tests: tResult.data,
      course: cResult.data
    })
  }
};

export const getCertificate = (courseId) => {
  return async (dispatch) => {
    dispatch({
      type: types.CERTIFICATE_PENDING
    });

    const certificateUrl = `/api/courses/${courseId}/certificate`;
    const [error, result] = await to(get(certificateUrl, {
      responseType: 'arraybuffer'
    }));

    if (error) {
      return dispatch({
        type: types.CERTIFICATE_ERROR
      })
    }

    const { data, status } = result;
    if (status === 204) {
      return dispatch({
        type: types.CERTIFICATE_SUCCESS_NO_LOAD
      })
    }

    return dispatch({
      type: types.CERTIFICATE_SUCCESS_LOAD,
      certificate: data
    })
  }
};

export const resetTests = () => {
  return (dispatch) => {
    dispatch({
      type: types.CANDIDATE_TEST_FLUSH
    })
  }
};
