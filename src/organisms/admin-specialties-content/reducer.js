import { types } from "./actions";

const initState = {
  pending: false,
  success: false,
  error: false,
  courses: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case types.GET_COURSES_PENDING: {
      return {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.GET_COURSES_SUCCESS: {
      return {
        ...state,
        pending: false,
        success: true,
        error: false,
        courses: action.courses.sort().reverse()
      }
    }
    case types.GET_COURSES_ERROR: {
      return {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    case types.GET_COURSES_FLUSH: {
      return {
        ...state,
        pending: false,
        success: false,
        error: false,
        courses: []
      }
    }
    case types.CREATE_COURSE_PENDING: {
      return {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.CREATE_COURSE_SUCCESS: {
      return {
        ...state,
        pending: false,
        success: true,
        error: false,
        courses: [action.course, ...state.courses]
      }
    }
    case types.CREATE_COURSE_ERROR: {
      return {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    default: {
      return state
    }
  }
}