import { get, post, to } from "../../utils/apiServiceBase";
import history from "../../utils/history";

export const types = {
  GET_TESTS_PENDING: 'GET_TESTS_PENDING',
  GET_TESTS_SUCCESS: 'GET_TESTS_SUCCESS',
  GET_TESTS_ERROR: 'GET_TESTS_ERROR',
  GET_TESTS_FLUSH: 'GET_TESTS_FLUSH',
  CREATE_TEST_PENDING: 'CREATE_TEST_PENDING',
  CREATE_TEST_SUCCESS: 'CREATE_TEST_SUCCESS',
  CREATE_TEST_ERROR: 'CREATE_TEST_ERROR'
};

export const getTests = () => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_TESTS_PENDING
    });

    const testsUrl = '/api/tests/all';
    const [error, result] = await to(get(testsUrl));

    if (error) {
      return dispatch({
        type: types.GET_TESTS_ERROR
      })
    }

    return dispatch({
      type: types.GET_TESTS_SUCCESS,
      tests: result.data
    })
  }
};

export const createTest = (test) => {
  return async (dispatch) => {
    dispatch({
      type: types.CREATE_TEST_PENDING
    });

    const testsUrl = '/api/tests';
    const [error, result] = await to(post(testsUrl, test));

    if (error) {
      return dispatch({
        type: types.CREATE_TEST_ERROR
      });
    }

    dispatch({
      type: types.CREATE_TEST_SUCCESS,
      test: result.data
    });
    history.push(`/admin/tests-control/${result.data.testId}/questions`)
  }
};

export const reset = () => {
  return (dispatch) => {
    dispatch({
      type: types.GET_TESTS_FLUSH
    })
  }
};