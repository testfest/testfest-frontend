import jwt from "jsonwebtoken";

import { constants } from "../../utils/constants";
import * as paths from '../../utils/paths'
import history from "../../utils/history";

export const types = {
  LOAD_USER: 'LOAD_USER',
  LOAD_ERROR: 'LOAD_ERROR',
  FLUSH_USER: 'FLUSH_USER'
};

export const loadUser = () => {
  return (dispatch) => {
    const token = localStorage.getItem('token');

    let parsedToken;
    try {
      parsedToken = jwt.verify(token, constants.JWT_KEY);
    } catch (error) {
      history.push(paths.LOGIN);
      return
    }

    return dispatch({
      type: types.LOAD_USER,
      token: parsedToken
    })
  }
};

export const reset = () => {
  return (dispatch) => {
    dispatch({
      type: types.FLUSH_USER
    })
  }
};