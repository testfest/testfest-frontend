import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";

import { getTest, submitQuestions } from './actions';
import Sidebar from "../../molecules/sidebar/Sidebar";
import Button from "../../atoms/button/Button";
import DeleteQuestionIcon from "../../atoms/delete-question-icon/DeleteQuestionIcon";
import QuestionForm from "../../molecules/question-form/QuestionForm";

import './AdminQuestionContent.css';


class AdminQuestionContent extends Component {
  constructor(props) {
    super(props);
    this.defaultQuestion = {
      id: 0,
      text: '',
      points: 0,
      answers: [
        {
          answer: '',
          valid: false
        },
        {
          answer: '',
          valid: false
        },
        {
          answer: '',
          valid: false
        }
      ]
    };
    this.totalScore = 5;
    this.state = {
      questions: [this.defaultQuestion],
      currentQuestion: this.defaultQuestion,
      currentQuestionNumber: 1,
      scoreRest: this.totalScore - this.defaultQuestion.points,
      previousPoints:  this.defaultQuestion.points
    };
    this.goToPreviousQuestion = this.goToPreviousQuestion.bind(this);
    this.goToNextQuestion = this.goToNextQuestion.bind(this);
    this.createTest = this.createTest.bind(this);
    this.handleNewQuestion = this.handleNewQuestion.bind(this);
    this.handleNewAnswer = this.handleNewAnswer.bind(this);
    this.removeLastAnswer = this.removeLastAnswer.bind(this);
    this.removeQuestion = this.removeQuestion.bind(this);
    this.selectQuestion = this.selectQuestion.bind(this);
    this.setQuestionText = this.setQuestionText.bind(this);
    this.setQuestionPoints = this.setQuestionPoints.bind(this);
    this.setAnswer = this.setAnswer.bind(this);
  }

  componentDidMount() {
    const { getTest, testId } = this.props;
    getTest(testId);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.success !== this.props.success) {
      const { test } = this.props;
      const { points } = test;

      this.setState({
        scoreRest: points
      })
    }
  }

  setQuestionText(event) {
    const { value: newValue } = event.target;
    const { questions, currentQuestionNumber } = this.state;
    questions[currentQuestionNumber - 1].text = newValue;
    this.setState({
      questions: questions
    })
  }

  setQuestionPoints(event) {
    const newValue = +event.target.value;
    const { questions, currentQuestionNumber, scoreRest, previousPoints } = this.state;

    if (scoreRest + previousPoints - newValue < 0) {
      this.setState(prevState => ({
        currentQuestion: prevState.currentQuestion
      }));
      alert('Распредилите баллы так, чтобы остаток не был отрицательным');
      return;
    }

    questions[currentQuestionNumber - 1].points = newValue;
    this.setState({
      questions: questions,
      scoreRest: scoreRest + previousPoints - newValue,
      previousPoints: newValue
    });
  }

  setAnswer(event) {
    const { target } = event;
    const name = target.name;
    const indexOfNumber = name.search(/\d+/);
    const propertyName = name.substring(0, indexOfNumber);
    const answerIndex = +name.substring(indexOfNumber);

    const createDictionary = () => {
      const dictionaryProperties = ['Верно', 'Неверно'];
      return {
        [dictionaryProperties[0]]: true,
        [dictionaryProperties[1]]: false
      };
    };

    let newValue = target.value;
    if (propertyName === 'valid') {
      const dictionary = createDictionary();
      newValue = dictionary[newValue];
    }
    const { questions, currentQuestionNumber } = this.state;
    questions[currentQuestionNumber - 1].answers[answerIndex][propertyName] = newValue;

    this.setState({
      questions: questions
    })
  }

  handleNewQuestion() {
    this.setState(prevState => ({
      questions: [
        ...prevState.questions,
        {
          id: prevState.questions[prevState.questions.length - 1].id + 1,
          text: '',
          points: 0,
          answers: [
            {
              answer: '',
              valid: false
            },
            {
              answer: '',
              valid: false
            },
            {
              answer: '',
              valid: false
            }
          ]
        }
      ]
    }))
  }

  handleNewAnswer() {
    const { currentQuestion } = this.state;
    currentQuestion.answers.push({
      answer: '',
      valid: false
    });
    this.setState({
      currentQuestion: currentQuestion
    })
  }

  removeLastAnswer() {
    const { currentQuestion } = this.state;
    currentQuestion.answers.pop();
    this.setState({
      currentQuestion: currentQuestion
    })
  }

  removeQuestion(event, id) {
    let { questions, currentQuestion } = this.state;
    if (questions.length === 1) {
      alert('Нельзя удалять единственный вопрос');
      event.stopPropagation();
      return;
    }

    if (currentQuestion.id === id) {
      alert('Нельзя удалить открытый вопрос');
      event.stopPropagation();
      return;
    }

    questions = questions.filter(question => question.id !== id);
    this.setState({
      questions: questions
    });
    event.stopPropagation();
  }

  goToPreviousQuestion() {
    if (this.state.currentQuestionNumber === 1) {
      return;
    }

    const { questions, currentQuestionNumber } = this.state;
    this.setState({
      currentQuestion: questions[currentQuestionNumber - 2],
      currentQuestionNumber: currentQuestionNumber - 1,
      previousPoints:  questions[currentQuestionNumber - 2].points
    });
  }

  goToNextQuestion() {
    const questionsCount = this.state.questions.length;
    if (this.state.currentQuestionNumber === questionsCount) {
      return;
    }

    const { questions, currentQuestionNumber } = this.state;
    this.setState({
      currentQuestion: questions[currentQuestionNumber],
      currentQuestionNumber: currentQuestionNumber + 1,
      previousPoints:  questions[currentQuestionNumber].points
    });
  }

  createTest() {
    const { questions } = this.state;
    const { testId, submitQuestions } = this.props;
    if (!this.areQuestionsCorrect()) {
      return;
    }
    const formatQuestions = question => ({
      text: question.text,
      points: question.points,
      answers: question.answers
    });

    submitQuestions(testId, questions.map(formatQuestions));
  }

  selectQuestion(selectedQuestionNumber) {
    const { questions } = this.state;
    const selectedQuestion = questions[selectedQuestionNumber - 1];
    this.setState({
      currentQuestion: selectedQuestion,
      currentQuestionNumber: selectedQuestionNumber,
      previousPoints:  selectedQuestion.points
    })
  }

  areQuestionsCorrect() {
    for (let i = 0; i < this.state.questions.length; i++) {
      const { text, points, answers } = this.state.questions[i];
      if (text === '') {
        alert(`Введите текст вопроса ${i + 1}`);
        return false;
      }

      if (points === 0) {
        alert(`Введите количество баллов за вопрос ${i + 1}`);
        return false;
      }

      let hasTrueAnswer = false;
      for (let answerObject of answers) {
        hasTrueAnswer = hasTrueAnswer || answerObject.valid;
        if (answerObject.answer === '') {
          alert(`Введите текст для всех ответов в вопросе ${i + 1}`);
          return false;
        }
      }

      if (!hasTrueAnswer) {
        alert(`Хотя бы один ответ должен быть верным в вопросе ${i + 1}`);
        return false;
      }
    }

    if (this.state.scoreRest > 0) {
      alert('Не все баллы распределены по вопросам');
      return;
    }

    return true;
  }

  render() {
    const { test } = this.props;
    const { testName } = test;
    const { currentQuestion, currentQuestionNumber, scoreRest, questions } = this.state;

    let nextButtonText;
    let nextButtonClickHandler;
    if (currentQuestionNumber === questions.length) {
      nextButtonText = 'Завершить';
      nextButtonClickHandler = this.createTest;
    } else {
      nextButtonText = 'Далее';
      nextButtonClickHandler = this.goToNextQuestion;
    }

    const sidebarContent = [{
      content: 'Добавить',
      onSelect: this.handleNewQuestion
    }];
    for (let i = 1; i <= questions.length; i++) {
      sidebarContent.push({
        content: <p className='admin-question-content__sidebar-item'>
          Вопрос {i}
          <DeleteQuestionIcon
            key={i}
            questionId={questions[i - 1].id}
            onClick={this.removeQuestion}/>
        </p>,
        questionNumber: i,
        onSelect: this.selectQuestion
      });
    }
    sidebarContent.push({
      content: 'Завершить',
      onSelect: this.createTest
    });

    return (
      <div className="admin-question-content">
        <Sidebar items={sidebarContent} selectedQuestionNumber={currentQuestionNumber}/>
        <h1 className="admin-question-content__title">{testName}</h1>
        <p className="admin-question-content__question-number">Вопрос {currentQuestionNumber}</p>
        <p className="admin-question-content__score-rest">Осталось баллов: {scoreRest}</p>
        <QuestionForm
          question={currentQuestion}
          onAddNewAnswer={this.handleNewAnswer}
          onRemoveLastQuestion={this.removeLastAnswer}
          onChangeAnswer={this.setAnswer}
          onChangeText={this.setQuestionText}
          onChangePoints={this.setQuestionPoints}/>
        <div className="admin-question-content__buttons-container">
          <Button
            handleClick={this.goToPreviousQuestion}
            classNames={'button_size_middle button_center'}
          >
            Назад
          </Button>
          <Button
            handleClick={nextButtonClickHandler}
            classNames={'button_size_middle button_center'}
          >
            {nextButtonText}
          </Button>
        </div>
      </div>
    )
  }
}

AdminQuestionContent.propTypes = {
  testId: PropTypes.string.isRequired
};

const mapStateToProps = (state) => ({
  test: state.adminQuestionsReducer.test,
  success: state.adminQuestionsReducer.success
});

const mapDispatchToProps = {
  getTest,
  submitQuestions
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminQuestionContent);
