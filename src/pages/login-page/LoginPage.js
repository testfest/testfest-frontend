import React, { Fragment, Component } from 'react';
import Header from "../../molecules/header/Header";
import LoginContent from "../../organisms/login-content/LoginContent";
import Footer from "../../molecules/footer/Footer";


class LoginPage extends Component {
  componentDidMount() {
    localStorage.removeItem('token');
  }

  render() {
    return (
      <Fragment>
        <Header/>
        <LoginContent/>
        <Footer/>
      </Fragment>
    );
  }
}

export default LoginPage;
