export const ROOT = '/';
export const LOGIN = '/login';
export const COURSES = '/courses';
export const TESTS = '/tests';
export const TESTS_FULL = `/:course${TESTS}`;
export const CANDIDATE_TEST = `${TESTS_FULL}/:test`;

export const ADMIN = '/admin';
export const ADMIN_COURSES = `${ADMIN}/courses-control`;
export const ADMIN_TESTS = `${ADMIN}/tests-control`;
export const ADMIN_QUESTIONS = `${ADMIN}/tests-control/:test/questions`;

export const FORBIDDEN = '/403';