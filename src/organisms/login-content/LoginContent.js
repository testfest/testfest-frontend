import React from 'react';
import LoginForm from "../../molecules/login-form/LoginForm";
import RegistrationForm from "../../molecules/registration-form/RegistrationForm";
import Tabs from "../../molecules/tabs/Tabs";
import problemsImage from './images/problems.png';
import './LoginContent.css';

const LoginContent = () => {
  const loginForm = <LoginForm/>;
  const registrationForm = <RegistrationForm/>;
  const tabsInfo = [
    {
      id: 0,
      title: "Вход",
      isActive: true,
      connectedElement: loginForm
    },
    {
      id: 1,
      title: "Регистрация",
      isActive: false,
      connectedElement: registrationForm
    }
  ];

  return (
    <main className="login-content">
      <Tabs tabsInfo={tabsInfo}/>
      <article className="login-content__site-description">
        <p>В современном российском обществе существуют проблемы трудоустройства, как у работодателей, так и у
          соискателей:</p>
        <img className="login-content__image" src={problemsImage} alt="Проблемы трудоустройства"/>
        <ul className="login-content_problems-list">
          <li className="login-content__problem">Текущие российские институты образования не соответствуют ожиданиям людей по качеству преподавания и
            релевантности знаний.</li>
          <li className="login-content__problem">Есть люди, которые решают обучиться профессии сами, без формального обучения.</li>
          <li className="login-content__problem">Для работодателя нет никаких гарантий того, что кандидат обладает требованиями, которые предъявлены к
            профессии. Приходится самим проводить сертификацию, тратить время на собеседования. Иногда это частично
            заменяют требованием к формальному образованию – и тогда теряется часть кандидатов.</li>
          <li className="login-content__problem">Соискателю, особенно без формального образования, труднее найти место работы.</li>
        </ul>

        <p>Мы предлагаем систему сертификации на соответствие уровню знаний и навыков, предъявляемым для работы по
          специальностям. Помимо среды прохождения сертификации мы разрабатываем и саму методику, которая соответствует
          требованиям большинства компаний на рынке труда.</p>
        <ul>
          <li className="login-content__decision">Для работодателей наша система гарантирует проверку знаний и навыков кандидатов.</li>
          <li className="login-content__decision">Для соискателей наша система даёт возможность доказать уровень своих знаний и навыков и получить подтверждение
            прохождения сертификации.</li>
        </ul>
      </article>
    </main>
  );
};

export default LoginContent;
