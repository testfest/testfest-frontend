import React, { Component } from 'react';

import * as paths from '../../utils/paths';

import './style.css';


class NotFoundPage extends Component {
  render() {
    return (
      <main className='not-found-page'>
        <header className='not-found-page__code'>404</header>
        <div className='not-found-page__message'>Запрошенная страница не была найдена</div>
        <div>
          <a href={paths.LOGIN}>Вернуться на страницу входа</a>
        </div>
      </main>
    );
  }
}

export default NotFoundPage;