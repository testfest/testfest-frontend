import React from 'react';
import './Footer.css';

const Footer = () => {
  return (
    <footer className="footer vcard">
      <p>Разработчики -&nbsp;
        <span className="given-name">Александр</span> <span className="family-name">Ионов</span>,&nbsp;
        <span className="given-name">Елизавета</span> <span className="family-name">Казанкова</span>
      </p>
      <p className="adr">
        <span className="country-name">Россия</span>,&nbsp;
        <span className="locality">Омск</span>,&nbsp;
        <a
          className="footer__link"
          href="https://omgtu.ru/general_information/faculties/faculty_of_information_technology_and_computer_systems/department_of_automated_systems_of_information_processing_and_management/"
          title="Страница кафедры АСОИУ на сайте ОмГТУ">Кафедра АСОИУ в ОмГТУ</a></p>
    </footer>
  )
};

export default Footer;
