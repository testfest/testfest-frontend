import { combineReducers } from "redux";

import loginReducer from '../molecules/login-form/reducer'
import registerReducer from '../molecules/registration-form/reducer'
import coursesReducer from '../organisms/specialty-choice-content/reducer'
import headerReducer from '../organisms/user-header/reducer'
import adminCoursesReducer from '../organisms/admin-specialties-content/reducer'
import adminTestsReducer from '../organisms/admin-tests-content/reducer'
import adminQuestionsReducer from '../organisms/admin-question-content/reducer'
import { certificateReducer, testsReducer } from "../organisms/candidate-tests-content/reducer";
import { questionsReducer, resultsReducer } from "../organisms/candidate-question-content/reducer";


const appReducer = combineReducers({
  adminCoursesReducer,
  adminTestsReducer,
  adminQuestionsReducer,
  headerReducer,
  registerReducer,
  coursesReducer,
  testsReducer,
  certificateReducer,
  questionsReducer,
  resultsReducer,
  loginReducer
});

export default (state = {}, action) => {
  return appReducer(state, action);
};