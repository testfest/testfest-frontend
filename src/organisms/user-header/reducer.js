import { types } from "./actions";
import { constants } from "../../utils/constants";


const initState = {
  firstName: '',
  lastName: ''
};

export default (state = initState, action) => {
  switch (action.type) {
    case types.LOAD_USER: {
      const { token } = action;

      return {
        ...state,
        firstName: token[constants.FIRST_NAME_KEY],
        lastName: token[constants.LAST_NAME_KEY]
      }
    }
    case types.FLUSH_USER: {
      return {
        ...state,
        firstName: '',
        lastName: ''
      }
    }
    default: {
      return state;
    }
  }
}
