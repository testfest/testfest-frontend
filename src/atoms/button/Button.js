import React from 'react';
import PropTypes from 'prop-types';
import './Button.css';

const Button = ({ classNames, handleClick, children }) => {
  const classList = `button ${classNames}`;
  return (
    <button
      className={classList}
      onClick={handleClick}>
      {children}</button>
  );
};

Button.propTypes = {
  classNames: PropTypes.string,
  handleClick: PropTypes.func
};

Button.defaultProps = {
  classNames: '',
  handleClick: () => {}
};

export default Button;
