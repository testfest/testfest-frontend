import { get, post, to } from "../../utils/apiServiceBase";

export const types = {
  GET_COURSES_PENDING: 'GET_COURSES_PENDING',
  GET_COURSES_SUCCESS: 'GET_COURSES_SUCCESS',
  GET_COURSES_ERROR: 'GET_COURSES_ERROR',
  GET_COURSES_FLUSH: 'GET_COURSES_FLUSH',
  CREATE_COURSE_PENDING: 'CREATE_COURSE_PENDING',
  CREATE_COURSE_SUCCESS: 'CREATE_COURSE_SUCCESS',
  CREATE_COURSE_ERROR: 'CREATE_COURSE_ERROR'
};

export const getCourses = () => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_COURSES_PENDING
    });

    const coursesUrl = '/api/courses';
    const [error, result] = await to(get(coursesUrl));

    if (error) {
      return dispatch({
        type: types.GET_COURSES_ERROR
      })
    }

    return dispatch({
      type: types.GET_COURSES_SUCCESS,
      courses: result.data
    })
  }
};

export const createCourse = (course) => {
  return async (dispatch) => {
    dispatch({
      type: types.CREATE_COURSE_PENDING
    });

    const coursesUrl = '/api/courses';
    const [error, result] = await to(post(coursesUrl, course));

    if (error) {
      return dispatch({
        type: types.CREATE_COURSE_ERROR
      })
    }

    return dispatch({
      type: types.CREATE_COURSE_SUCCESS,
      course: result.data
    })
  }
};

export const reset = () => {
  return (dispatch) => {
    dispatch({
      type: types.GET_COURSES_FLUSH
    })
  }
};