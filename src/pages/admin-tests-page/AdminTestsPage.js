import React, { Fragment } from 'react';
import Header from "../../molecules/header/Header";
import Menu from "../../molecules/menu/Menu";
import AdminTestsContent from "../../organisms/admin-tests-content/AdminTestsContent";

const AdminTestsPage = () => {
  return (
    <Fragment>
      <Header>
        <Menu/>
      </Header>
      <AdminTestsContent/>
    </Fragment>
  )
};

export default AdminTestsPage;
