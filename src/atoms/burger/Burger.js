import React, { Component } from 'react';
import PropTypes from 'prop-types';
import clsx from "clsx";
import './Burger.css';

class Burger extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCross: false
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(prevState => {
      return {
        isCross: !prevState.isCross
      }
    });
    this.props.onClick();
  }

  render() {
    const { isCross } = this.state;
    const classList = clsx('burger', isCross && 'burger_changed');

    return (
      <div className={classList} onClick={this.handleClick}>
        <div className="burger__strip burger__strip_position_top"/>
        <div className="burger__strip burger__strip_position_middle"/>
        <div className="burger__strip burger__strip_position_bottom"/>
      </div>
    )
  }
}

Burger.propTypes = {
  onClick: PropTypes.func.isRequired
};

export default Burger;
