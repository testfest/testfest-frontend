import React, { Fragment, Component } from 'react';
import { withRouter } from "react-router-dom";

import CandidateQuestionContent from "../../organisms/candidate-question-content/CandidateQuestionContent";
import UserHeader from "../../organisms/user-header/UserHeader";

class CandidateQuestionPage extends Component {
  render() {
    const { match } = this.props;
    const { test, course } = match.params;

    return(
      <Fragment>
        <UserHeader/>
        <CandidateQuestionContent testId={test} courseId={course}/>
      </Fragment>
    )
  }
}

export default withRouter(CandidateQuestionPage);
