import React from 'react';
import PropTypes from 'prop-types'

import './style.css';


const ErrorMessage = (props) => {
  const { children, className, display } = props;

  return (
    <div className={`error-message ${className}`}>
      {display && children}
    </div>
  )
};

ErrorMessage.propTypes = {
  children: PropTypes.node,
  display: PropTypes.bool,
  className: PropTypes.string
};

ErrorMessage.defaultProps = {
  className: '',
  display: false
};

export default ErrorMessage;