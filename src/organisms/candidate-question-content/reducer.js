import { types } from "./actions";

const initQuestionsState = {
  pending: false,
  success: false,
  error: false,
  test: {},
  questions: []
};
const initResultsReducer = {
  pending: false,
  success: false,
  error: false
};

export const questionsReducer = (state = initQuestionsState, action) => {
  switch (action.type) {
    case types.GET_QUESTIONS_PENDING: {
      return {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.GET_QUESTIONS_SUCCESS: {
      return {
        ...state,
        pending: false,
        success: true,
        error: false,
        test: action.test,
        questions: action.questions
      }
    }
    case types.GET_QUESTIONS_ERROR: {
      return {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    case types.GET_QUESTIONS_FLUSH: {
      return {
        ...state,
        pending: false,
        success: false,
        error: false,
        test: {},
        questions: []
      }
    }
    default: {
      return state;
    }
  }
};

export const resultsReducer = (state = initResultsReducer, action) => {
  switch (action.type) {
    case types.SUBMIT_RESULTS_PENDING: {
      return {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.SUBMIT_RESULTS_SUCCESS: {
      return {
        ...state,
        pending: false,
        success: true,
        error: false
      }
    }
    case types.SUBMIT_RESULTS_ERROR: {
      return {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    default: {
      return state;
    }
  }
};