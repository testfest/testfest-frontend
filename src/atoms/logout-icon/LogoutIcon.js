import React from 'react';

import history from "../../utils/history";

import './LogoutIcon.css';


const LogoutIcon = () => {
  const logout = () => {
    localStorage.removeItem('token');
    history.push('/login')
  };

  return (
    <div className="logout-icon" onClick={logout}/>
  )
};

export default LogoutIcon;
