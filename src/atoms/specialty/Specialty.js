import React from 'react';
import PropTypes from 'prop-types';
import './Specialty.css';

const Specialty = (props) => {
  const { name, description, testsCount, onClick } = props;

  const chooseEnding = number => {
    const lastDigit = number % 10;
    if ((11 <= number && number <= 14) ||
      (5 <= lastDigit && lastDigit <= 9) ||
      lastDigit === 0) {
      return 'ов';
    }
    if (2 <= lastDigit && lastDigit <= 4) {
      return 'а';
    }
    return '';
  };

  return (
    <article className="specialty" onClick={onClick}>
      <h2 className="specialty__name">{name}</h2>
      <p>{description}</p>
      <p className="specialty__tests-count">{`${testsCount} тест${chooseEnding(testsCount)}`}</p>
    </article>
  )
};

Specialty.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  description: PropTypes.string,
  testsCount: PropTypes.number,
  onClick: () => {}
};

Specialty.defaultProps = {
  id: '',
  name: 'Course name',
  description: 'Description',
  testsCount: 0,
  onClick: () => {}
};

export default Specialty;
