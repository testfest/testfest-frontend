import React from 'react';
import PropTypes from 'prop-types';
import deleteIcon from './images/delete.svg';
import './DeleteQuestionIcon.css';

const DeleteQuestionIcon = ({ questionId, onClick }) => {
  const handleClick = (event) => {
    onClick(event, questionId);
  };

  return (
    <img className="delete-question-icon" src={deleteIcon} alt="del" onClick={handleClick}/>
  )
};

DeleteQuestionIcon.propTypes = {
  questionId: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired
};

export default DeleteQuestionIcon;
