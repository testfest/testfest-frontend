import { postNoToken, to } from "../../utils/apiServiceBase";

export const types = {
  LOGIN_PENDING: 'LOGIN_PENDING',
  LOGIN_ERROR: 'LOGIN_ERROR',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  LOGIN_FLUSH: 'LOGIN_FLUSH'
};

export const login = (username, password) => {
  return async (dispatch) => {
    dispatch({
      type: types.LOGIN_PENDING
    });

    const body = {
      username: username,
      password: password
    };

    const [error, result] = await to(postNoToken('/api/sign-in', body));

    if (error) {
      return dispatch({
        type: types.LOGIN_ERROR
      });
    }

    return dispatch({
      type: types.LOGIN_SUCCESS,
      token: result.token
    })
  }
};

export const reset = () => {
  return (dispatch) => {
    dispatch({
      type: types.LOGIN_FLUSH
    })
  }
};