import { types } from "./actions";

const initState = {
  pending: false,
  success: false,
  error: false,
  test: {}
};

export default (state = initState, action) => {
  switch (action.type) {
    case types.GET_TEST_PENDING: {
      return {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.GET_TEST_SUCCESS: {
      return {
        ...state,
        pending: false,
        success: true,
        error: false,
        test: action.test
      }
    }
    case types.GET_TEST_ERROR: {
      return {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    case types.GET_TEST_FLUSH: {
      return {
        ...state,
        pending: false,
        success: false,
        error: false,
        test: {}
      }
    }
    default: {
      return state;
    }
  }
}