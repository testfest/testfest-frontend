import React, { Component } from 'react';
import { connect } from "react-redux";

import { getCourses, selectCourse } from './actions';
import Specialty from "../../atoms/specialty/Specialty";

import './SpecialtyChoiceContent.css';


class SpecialtyChoiceContent extends Component {
  componentDidMount() {
    this.props.getCourses()
  }

  render() {
    const { courses, selectCourse } = this.props;

    const mapCourses = course => {
      const { id, courseName, courseDescription, testsCount } = course;

      const onClick = () => {
        selectCourse(id);
      };

      return (
        <Specialty
          key={id}
          name={courseName}
          description={courseDescription}
          testsCount={testsCount}
          onClick={onClick}
        />
      );
    };

    return (
      <div className="specialty-choice-content">
        <h1 className="specialty-choice-content__heading">Выберите специализацию</h1>
        <div className="specialty-choice-content__specialties">
          {courses.map(mapCourses)}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  courses: state.coursesReducer.courses
});

const mapDispatchToProps = {
  getCourses,
  selectCourse
};

export default connect(mapStateToProps, mapDispatchToProps)(SpecialtyChoiceContent);
