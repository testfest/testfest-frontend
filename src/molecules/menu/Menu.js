import React from 'react';
import clsx from "clsx";

import * as paths from '../../utils/paths'
import Burger from "../../atoms/burger/Burger";

import './Menu.css';

class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false
    };
    this.menuItems = ['Тесты', 'Специальности', 'Выйти'];
    this.hrefs = [paths.ADMIN_TESTS, paths.ADMIN_COURSES, paths.LOGIN];
    this.toggleMenu = this.toggleMenu.bind(this);
  }

  toggleMenu() {
    this.setState(prevState => {
      return {
        visible: !prevState.visible
      }
    })
  }

  render() {
    const createMenuItemElement = (menuItem, href) => {
      return <li key={menuItem} className="menu__item">
        <a className="menu__link" href={href}>{menuItem}</a>
      </li>
    };

    const menuItemElements = [];
    for (let i = 0; i < this.menuItems.length; i++) {
      const menuItemElement = createMenuItemElement(this.menuItems[i], this.hrefs[i]);
      menuItemElements.push(menuItemElement);
    }

    const { visible } = this.state;
    const classList = clsx('menu', visible && 'menu_visible');

    return(
      <nav>
        <ul className={classList}>
          {menuItemElements}
        </ul>
        <Burger onClick={this.toggleMenu}/>
      </nav>
    );
  }
}

export default Menu;
