import React from "react";
import PropTypes from 'prop-types';
import clsx from "clsx";
import './Tab.css';

const Tab = ({ title, isActive, handleClick }) => {
  let classList = clsx('tab', isActive && 'tab_active');
  return (
    <div
      className={classList}
      onClick={handleClick}>
      {title}
    </div>
  );
};

Tab.propTypes = {
  title: PropTypes.string.isRequired,
  isActive: PropTypes.bool.isRequired,
  handleClick: PropTypes.func.isRequired
};

export default Tab;
