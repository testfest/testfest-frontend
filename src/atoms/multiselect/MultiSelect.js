import React from 'react';
import PropTypes from 'prop-types';
import './Multiselect.css';

const MultiSelect = ({ name, options, classNames, onChange }) => {
  const classList = `multiselect ${classNames}`;

  const createOption = ({ key, value }) => {
    return <option
      key={key}
      className="multiselect__option"
      value={key}>
      {value}</option>
  };
  const optionElements = options.map(createOption);

  return (
    <select
      className={classList}
      name={name}
      onChange={onChange}
      multiple={true}
      size={4}>
      {optionElements}
    </select>
  )
};

MultiSelect.propTypes = {
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.object.isRequired).isRequired,
  classNames: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

export default MultiSelect;
