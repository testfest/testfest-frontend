import React from 'react';
import PropTypes from 'prop-types';
import clsx from "clsx";
import './SidebarItem.css';

const SidebarItem = ({ isSelected, questionNumber, select, children }) => {
  const classList = clsx('sidebar-item', isSelected && 'sidebar-item_selected');
  const handleClick = () => {
    if (questionNumber) {
      select(questionNumber);
    } else {
      select();
    }
  };

  return (
    <li className={classList} onClick={handleClick}>
      {children}
    </li>
  )
};

SidebarItem.propTypes = {
  isSelected: PropTypes.bool.isRequired,
  questionNumber: PropTypes.number,
  select: PropTypes.func.isRequired,
};

export default SidebarItem;
