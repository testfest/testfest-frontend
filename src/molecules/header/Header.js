import React from 'react';
import logo from './images/logo.png';
import './Header.css';

const Header = ({ children }) => {
  return (
    <header className="header">
      <img
        className="header__logo"
        src={logo}
        alt="TestFest logo"/>
      <div className="header__info">{children}</div>
    </header>
  )
};

export default Header;
