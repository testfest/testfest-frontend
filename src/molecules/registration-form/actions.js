import { postNoToken, to } from "../../utils/apiServiceBase";

export const types = {
  REGISTER_PENDING: 'REGISTER_PENDING',
  REGISTER_SUCCESS: 'REGISTER_SUCCESS',
  REGISTER_ERROR: 'REGISTER_ERROR',
  REGISTER_FLUSH: 'REGISTER_FLUSH'
};

export const register = (data) => {
  return async (dispatch) => {
    dispatch({
      type: types.REGISTER_PENDING
    });

    const [error, result] = await to(postNoToken('/api/sign-up', data));

    if (error) {
      return dispatch({
        type: types.REGISTER_ERROR
      });
    }

    return dispatch({
      type: types.REGISTER_SUCCESS,
      token: result.token
    });
  }
};

export const reset = () => {
  return (dispatch) => {
    dispatch({
      type: types.REGISTER_FLUSH
    })
  }
};