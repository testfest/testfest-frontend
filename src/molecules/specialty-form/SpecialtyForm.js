import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TextInput from "../../atoms/text-input/TextInput";
import Button from "../../atoms/button/Button";

import './SpecialtyForm.css';


class SpecialtyForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: ''
    };
    this.setStateProperty = this.setStateProperty.bind(this);
    this.addSpecialty = this.addSpecialty.bind(this);
  }

  setStateProperty(event) {
    const {target} = event;
    const propertyName = target.name;
    const value = target.value;
    this.setState({
      [propertyName]: value
    })
  }

  addSpecialty(event) {
    event.preventDefault();
    const { onSubmit } = this.props;
    const isStateCorrect = () => {
      for (let property in this.state) {
        if (this.state[property] === '') {
          alert('Заполните все поля');
          return false;
        }
      }

      return true;
    };

    if (!isStateCorrect()) {
      return;
    }

    const { name, description } = this.state;
    this.setState({
      name: '',
      description: ''
    });
    onSubmit({
      courseName: name,
      courseDescription: description
    });
  }

  render() {
    const { name, description } = this.state;
    return (
      <form className="specialty-form">
        <TextInput
          handleChange={this.setStateProperty}
          type="text"
          name="name"
          value={name}
          classNames="text-input_size_full-width"
          placeholder="Название специализации"/>
        <TextInput
          handleChange={this.setStateProperty}
          type="textarea"
          name="description"
          value={description}
          classNames="text-input_size_full-width"
          placeholder="Описание"/>
        <Button handleClick={this.addSpecialty} classNames={'button_size_middle button_center'}>Добавить</Button>
      </form>
    )
  }
}

SpecialtyForm.propTypes = {
  onSubmit: PropTypes.func
};

SpecialtyForm.defaultProps = {
  onSubmit: () => {}
};

export default SpecialtyForm;
