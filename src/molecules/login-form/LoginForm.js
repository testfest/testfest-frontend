import React, { Component } from 'react';
import { connect } from "react-redux";

import { login, reset } from "./actions";
import { isEqual } from "../../utils/comparator";
import { roles } from "../../utils/constants";
import history from "../../utils/history";
import * as paths from '../../utils/paths';
import TextInput from "../../atoms/text-input/TextInput";
import Button from "../../atoms/button/Button";
import ErrorMessage from "../../atoms/error-message/ErrorMessage";
import FormField from "../form-field/FormField";

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      login: '',
      password: '',
      loginError: false
    };
    this.setStateProperty = this.setStateProperty.bind(this);
    this.login = this.login.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.success) {
      const { userRoles } = this.props;
      if (userRoles.some(role => role === roles.ROLE_ADMIN)) {
        history.push(paths.ADMIN_COURSES);
      } else {
        history.push(paths.COURSES);
      }
      this.props.reset();
    }
    if (!isEqual(prevProps, this.props) && this.props.error) {
      this.setState({
        loginError: true
      });
    }
  }

  setStateProperty(event) {
    const { target } = event;
    const propertyName = target.name;
    const value = target.value;
    this.setState({
      [propertyName]: value,
      loginError: false
    })
  }

  login(event) {
    event.preventDefault();
    if (!this.isStateCorrect()) {
      return;
    }
    const { login, password } = this.state;
    this.props.login(login, password);
  }

  isStateCorrect() {
    for (let property in this.state) {
      if (this.state[property] === '') {
        alert('Заполните все поля');
        return false;
      }
    }

    return true;
  }

  render() {
    const { loginError } = this.state;

    return (
      <form>
        <FormField title='Логин'>
          <TextInput
            type="text"
            classNames={'text-input_size_full-width'}
            name="login"
            handleChange={this.setStateProperty}
          />
        </FormField>
        <FormField title='Пароль'>
          <TextInput
            type="password"
            classNames={'text-input_size_full-width'}
            name="password"
            handleChange={this.setStateProperty}
          />
        </FormField>
        <ErrorMessage display={loginError}>
          Некорректный логин или пароль
        </ErrorMessage>
        <Button
          classNames={'button_size_full-width'}
          handleClick={this.login}
        >
          Войти
        </Button>
      </form>
    );
  }
}

const mapStateToProps = (state) => ({
  success: state.loginReducer.success,
  error: state.loginReducer.error,
  userRoles: state.loginReducer.userRoles
});

const mapDispatchToProps = {
  login,
  reset
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
