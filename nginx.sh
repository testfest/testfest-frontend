#!/bin/sh
docker run --rm --net=host -p 80:80 \
  -v $PWD/nginx.conf:/etc/nginx/conf.d/default.conf:ro \
  nginx:stable-alpine
