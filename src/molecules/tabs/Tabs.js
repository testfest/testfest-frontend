import React, {Component} from "react";
import PropTypes from 'prop-types';
import Tab from "./../../atoms/tab/Tab";
import './Tabs.css';

class Tabs extends Component {
  constructor(props) {
    super(props);
    const findActive = tab => tab.isActive;
    this.state = {
      tabsInfo: this.props.tabsInfo,
      activeTab: this.props.tabsInfo.find(findActive)
    };
    this.changeActiveTab = this.changeActiveTab.bind(this);
  }

  changeActiveTab(event) {
    const { target } = event;
    const findTab = tab => tab.title === target.textContent;
    const nextActiveTab = this.props.tabsInfo.find(findTab);
    if (nextActiveTab === this.state.activeTab) return;
    this.setState(state => {
      state.activeTab.isActive = false;
      nextActiveTab.isActive = true;
      return {
        activeTab: nextActiveTab
      }
    });
  }

  render() {
    const createTab = ({id, isActive, title}) => {
      return <Tab
        key={id}
        isActive={isActive}
        title={title}
        handleClick={this.changeActiveTab}/>
    };
    const tabs = this.state.tabsInfo.map(createTab);

    return (
      <div className="tabs">
        <div className="tabs__tab-panel">
          {tabs}
        </div>
        <div className="tabs_tab-content">
          {this.state.activeTab.connectedElement}
        </div>
      </div>
    )
  }
}

Tabs.propTypes = {
  tabsInfo: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    isActive: PropTypes.bool.isRequired,
    connectedElement: PropTypes.element.isRequired
  })).isRequired
};

export default Tabs;
