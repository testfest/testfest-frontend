import React, { Component } from 'react'
import { connect } from "react-redux";

import Header from "../../molecules/header/Header";
import { loadUser } from './actions';
import LogoutIcon from "../../atoms/logout-icon/LogoutIcon";


class UserHeader extends Component {
  componentDidMount() {
    this.props.loadUser()
  }

  render() {
    const { firstName, lastName } = this.props;

    return (
      <Header>
        <p>{firstName} {lastName}</p>
        <LogoutIcon/>
      </Header>
    );
  }
}

const mapStateToProps = (state) => ({
  firstName: state.headerReducer.firstName,
  lastName: state.headerReducer.lastName
});

const mapDispatchToProps = {
  loadUser
};

export default connect(mapStateToProps, mapDispatchToProps)(UserHeader);
