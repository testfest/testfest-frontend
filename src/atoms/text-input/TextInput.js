import React from 'react';
import PropTypes from 'prop-types';
import './TextInput.css';

const TextInput = ({ type, classNames, placeholder, value, name, handleChange }) => {
  const classList = `text-input ${classNames}`;

  let textInput;
  if (type === 'textarea') {
    textInput = <textarea
      className={classList}
      name={name}
      required={true}
      onChange={handleChange}
      value={value}
      placeholder={placeholder}/>
  } else {
    textInput = <input
      type={type}
      className={classList}
      name={name}
      value={value}
      placeholder={placeholder}
      required={true}
      onChange={handleChange}
      min={1}
      max={100}/>
  }

  return (
    textInput
  );
};

TextInput.defaultProps = {
  placeholder: '',
};

TextInput.propTypes = {
  type: PropTypes.oneOf(['text', 'password', 'date', 'textarea', 'number']).isRequired,
  classNames: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  handleChange: PropTypes.func.isRequired,
};

export default TextInput;
