import saveFile from 'file-saver'

import { types } from "./actions";


const initTestsState = {
  pending: false,
  success: false,
  error: false,
  course: {},
  tests: []
};

const initCertificateState = {
  pending: false,
  success: false,
  error: false
};

export const testsReducer = (state = initTestsState, action) => {
  switch (action.type) {
    case types.CANDIDATE_TESTS_PENDING: {
      return {
        ...state,
        pending: true,
        success: false,
        error: false
      }
    }
    case types.CANDIDATE_TESTS_SUCCESS: {
      return {
        ...state,
        pending: false,
        success: true,
        error: false,
        tests: action.tests,
        course: action.course
      }
    }
    case types.CANDIDATE_TESTS_ERROR: {
      return {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    case types.CANDIDATE_TEST_FLUSH: {
      return {
        ...state,
        pending: false,
        success: false,
        error: false,
        tests: [],
        course: {}
      }
    }
    default: {
      return state;
    }
  }
};

export const certificateReducer = (state = initCertificateState, action) => {
  switch (action.type) {
    case types.CERTIFICATE_PENDING: {
      return {
        ...state,
        pending: true,
        error: false,
        success: false
      }
    }
    case types.CERTIFICATE_SUCCESS_LOAD: {
      const file = new Blob([action.certificate], { type: 'application/pdf' });
      saveFile(file, 'certificate.pdf');

      return {
        ...state,
        pending: false,
        error: false,
        success: true
      }
    }
    case types.CERTIFICATE_SUCCESS_NO_LOAD: {
      return {
        ...state,
        pending: false,
        error: false,
        success: true
      }
    }
    case types.CERTIFICATE_ERROR: {
      return {
        ...state,
        pending: false,
        success: false,
        error: true
      }
    }
    default: {
      return state;
    }
  }
};
