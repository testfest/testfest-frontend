import axios from 'axios'

import * as paths from './paths'
import history from "./history";
import jwt from "jsonwebtoken";
import { constants } from "./constants";


export const initInterceptor = () => {
  axios.interceptors.request.use((config) => {
    const token = localStorage.getItem('token');

    config.headers = {
      ...config.headers,
      ...{
        Authorization: `Bearer ${token}`
      }
    };

    return config;
  });

  axios.interceptors.response.use(
    response => response,
    error => {
      const { response: { status } } = error;

      if (status === 401) {
        history.push(paths.LOGIN)
      }
    });
};

export const isAuthenticated = () => {
  const token = localStorage.getItem('token');

  try {
    jwt.verify(token, constants.JWT_KEY);
    return true;
  } catch (error) {
    return false;
  }
};

export const checkUserRole = (role) => {
  const token = localStorage.getItem('token');

  try {
    const parsedToken = jwt.verify(token, constants.JWT_KEY);
    return parsedToken[constants.USER_ROLE_KEY].split(',').some(userRole => userRole === role);
  } catch (error) {
    return false;
  }
};