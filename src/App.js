import React from 'react';
import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import { Router, Route, Redirect, Switch } from 'react-router-dom';
import thunk from "redux-thunk";

import history from "./utils/history";
import * as paths from './utils/paths'
import appReducer from "./utils/appReducer";
import { roles } from "./utils/constants";
import { checkUserRole, initInterceptor, isAuthenticated } from "./utils/tokenConfig";

import LoginPage from "./pages/login-page/LoginPage";
import SpecialtyChoicePage from "./pages/specialty-choice-page/SpecialtyChoicePage";
import CandidateTestsPage from "./pages/candidate-tests-page/CandidateTestsPage";
import CandidateQuestionPage from "./pages/candidate-question-page/CandidateQuestionPage";
import AdminSpecialtiesPage from "./pages/admin-specialties-page/AdminSpecialtiesPage";
import AdminQuestionPage from "./pages/admin-question-page/AdminQuestionPage";
import AdminTestsPage from "./pages/admin-tests-page/AdminTestsPage";
import NotFoundPage from "./pages/not-found-page/NotFoundPage";
import ForbiddenPage from "./pages/forbidden-page/ForbiddenPage";

const store = createStore(appReducer, applyMiddleware(thunk));

if (isAuthenticated()) {
  initInterceptor();
}

const checkAuth = (node) => {
  return isAuthenticated() ? node : <Redirect to={paths.LOGIN}/>
};

const checkRole = (node, role) => {
  return checkUserRole(role) ? node : <Redirect to={paths.FORBIDDEN}/>
};

const App = () => {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Switch>
          <Route exact path={paths.LOGIN} render={() => <LoginPage/>}/>
          <Route exact path={paths.ROOT} render={() => <Redirect to={paths.LOGIN}/>}/>
          <Route exact path={paths.COURSES} render={() => checkAuth(<SpecialtyChoicePage/>)}/>
          <Route exact path={paths.TESTS_FULL} render={() => checkAuth(<CandidateTestsPage/>)}/>
          <Route exact path={paths.CANDIDATE_TEST} render={() => checkAuth(<CandidateQuestionPage/>)}/>
          <Route
            exact
            path={paths.ADMIN_COURSES}
            render={() => checkRole(<AdminSpecialtiesPage/>, roles.ROLE_ADMIN)}
          />
          <Route
            exact
            path={paths.ADMIN_TESTS}
            render={() => checkRole(<AdminTestsPage/>, roles.ROLE_ADMIN)}
          />
          <Route
            exact
            path={paths.ADMIN_QUESTIONS}
            render={() => checkRole(<AdminQuestionPage/>, roles.ROLE_ADMIN)}
          />
          <Route exact path={'/403'} render={() => <ForbiddenPage/>}/>
          <Route render={() => checkAuth(<NotFoundPage/>)}/>
        </Switch>
      </Router>
    </Provider>
  );
};

export default App;
