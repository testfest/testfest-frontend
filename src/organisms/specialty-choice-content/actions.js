import { get, to } from "../../utils/apiServiceBase";
import history from "../../utils/history";

export const types = {
  COURSES_PENDING: 'COURSES_PENDING',
  COURSES_SUCCESS: 'COURSES_SUCCESS',
  COURSES_ERROR: 'COURSES_ERROR',
  COURSES_FLUSH: 'COURSES_FLUSH',
  COURSES_SELECTED: 'COURSES_SELECTED'
};

export const getCourses = () => {
  return async (dispatch) => {
    dispatch({
      type: types.COURSES_PENDING
    });

    const [error, result] = await to(get('/api/courses'));

    if (error) {
      dispatch({
        type: types.COURSES_ERROR
      });
    }

    if (result.status === 207) {
      history.push(`${result.data[0].id}/tests`);
      return;
    }

    dispatch({
      type: types.COURSES_SUCCESS,
      courses: result.data
    });
  }
};

export const selectCourse = (id) => {
  return async (dispatch) => {
    dispatch({
      type: types.COURSES_PENDING
    });

    const url = `/api/courses/${id}`;
    const [error] = await to(get(url));

    if (error) {
      return dispatch({
        type: types.COURSES_ERROR
      })
    }

    dispatch({
      type: types.COURSES_SELECTED
    });
    history.push(`/${id}/tests`);
  }
};

export const reset = () => {
  return (dispatch) => {
    dispatch({
      type: types.COURSES_FLUSH
    })
  }
};