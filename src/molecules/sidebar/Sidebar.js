import React from 'react';
import PropTypes from 'prop-types';
import SidebarItem from "../../atoms/sidebar-item/SidebarItem";
import './Sidebar.css';

const Sidebar = ({
                   items,
                   selectedQuestionNumber
}) => {
  const sidebarItems = [];
  for (let i = 0; i < items.length; i++) {
    const item = items[i];
    const isSelected = item.questionNumber === selectedQuestionNumber;
    sidebarItems.push(<SidebarItem
      key={i}
      select={item.onSelect}
      questionNumber={item.questionNumber}
      isSelected={isSelected}>
      {item.content}
    </SidebarItem>);
    console.log({selectedQuestionNumber});
  }

  return (
    <ul className="sidebar">
      {sidebarItems}
    </ul>
  );
};

Sidebar.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    content: PropTypes.any.isRequired,
    onSelect: PropTypes.func.isRequired,
    questionNumber: PropTypes.number
  }).isRequired,).isRequired,
  selectedQuestionNumber: PropTypes.number.isRequired,
};

export default Sidebar;
