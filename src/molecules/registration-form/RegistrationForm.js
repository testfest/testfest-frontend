import React, { Component } from 'react';
import { connect } from "react-redux";

import { register, reset } from "./actions";
import { isEqual } from "../../utils/comparator";
import history from "../../utils/history";
import * as paths from '../../utils/paths';
import TextInput from "../../atoms/text-input/TextInput";
import Button from "../../atoms/button/Button";
import ErrorMessage from "../../atoms/error-message/ErrorMessage";
import FormField from "../form-field/FormField";

class RegistrationForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      login: '',
      password: '',
      surname: '',
      name: '',
      patronymic: '',
      dateOfBirth: '',
      registerError: false
    };
    this.setStateProperty = this.setStateProperty.bind(this);
    this.register = this.register.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { success } = this.props;
    if (success) {
      history.push(paths.COURSES);
      this.props.reset();
    }
    if (!isEqual(prevProps, this.props) && this.props.error) {
      this.setState({
        registerError: true
      });
    }
  }

  setStateProperty(event) {
    const {target} = event;
    const propertyName = target.name;
    const value = target.value;
    this.setState({
      [propertyName]: value,
      registerError: false
    })
  }

  register(event) {
    event.preventDefault();
    if (!this.isStateCorrect()) {
      return;
    }
    const { login, password, surname, name, patronymic, dateOfBirth } = this.state
    const body = {
      username: login,
      password: password,
      firstName: name,
      middleName: patronymic,
      lastName: surname,
      birthDate: dateOfBirth
    };

    this.props.register(body);
  }

  isStateCorrect() {
    for (let property in this.state) {
      if (this.state[property] === '') {
        alert('Заполните все поля');
        return false;
      }
    }

    if (this.state.login.length < 4) {
      alert('Логин должен состоять хотя бы из 4 символов');
      return false;
    }

    if (this.state.password.length < 8) {
      alert('Пароль должен состоять хотя бы из 8 символов');
      return false;
    }

    const necessaryCharacters = [/.{8,}/, /[a-z]/, /[A-Z]/, /\d/, /-/];
    for (let i = 0; i < necessaryCharacters.length; i++) {
      if (!this.state.password.match(necessaryCharacters[i])) {
        alert('Пароль состоять из заглавных и строчных латинских букв, цифр и знака -');
        return false;
      }
    }

    return true;
  }

  render() {
    const { registerError } = this.state;

    return (
      <form>
        <FormField title='Логин'>
          <TextInput
            type="text"
            classNames={'text-input_size_full-width'}
            name="login"
            handleChange={this.setStateProperty}/>
        </FormField>
        <FormField title='Пароль'>
          <TextInput
            type="password"
            classNames={'text-input_size_full-width'}
            name="password"
            handleChange={this.setStateProperty}/>
        </FormField>
        <FormField title='Фамилия'>
          <TextInput
            type="text"
            classNames={'text-input_size_full-width'}
            name="surname"
            handleChange={this.setStateProperty}/>
        </FormField>
        <FormField title='Имя'>
          <TextInput
            type="text"
            classNames={'text-input_size_full-width'}
            name="name"
            handleChange={this.setStateProperty}/>
        </FormField>
        <FormField title='Отчество'>
          <TextInput
            type="text"
            classNames={'text-input_size_full-width'}
            name="patronymic"
            handleChange={this.setStateProperty}/>
        </FormField>
        <FormField title='Дата рождения'>
          <TextInput
            type="date"
            classNames={'text-input_size_full-width'}
            name="dateOfBirth"
            handleChange={this.setStateProperty}/>
        </FormField>
        <ErrorMessage display={registerError}>
          Не получается зарегистрировать пользователя с таким именем
        </ErrorMessage>
        <Button
          classNames={'button_size_full-width'}
          handleClick={this.register}>Зарегистрироваться</Button>
      </form>
    )
  }
}

const mapStateToProps = (state) => ({
  success: state.registerReducer.success,
  error: state.registerReducer.error
});

const mapDispatchToProps = {
  register,
  reset
};

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationForm);
