import { types } from "./actions";

const initState = {
  courses: [],
  success: false,
  pending: false,
  error: false
};

export default (state = initState, action) => {
  switch (action.type) {
    case types.COURSES_PENDING: {
      return {
        ...state,
        success: false,
        pending: true,
        error: false
      }
    }
    case types.COURSES_SUCCESS: {
      const filterCourses = (course) => course.testsCount !== 0;
      return {
        ...state,
        courses: action.courses.filter(filterCourses),
        success: true,
        pending: false,
        error: false
      }
    }
    case types.COURSES_ERROR: {
      return {
        ...state,
        success: false,
        pending: false,
        error: true
      }
    }
    case types.COURSES_SELECTED: {
      return {
        ...state,
        success: true,
        pending: false,
        error: false
      }
    }
    case types.COURSES_FLUSH: {
      return {
        ...state,
        success: false,
        pending: false,
        error: false,
        courses: []
      }
    }
    default: {
      return state;
    }
  }
}