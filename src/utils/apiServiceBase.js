import axios from 'axios'
import fetch from 'isomorphic-fetch';

export const to = (promise) => {
  return promise
    .then((data) => {
      return [null, data]
    })
    .catch((error) => [error])
};

export const get = (url, config) => {
  return axios.get(url, config)
};

export const post = (url, data, config) => {
  return axios.post(url, data, config)
};

export const postNoToken = (url, data) => {
  return fetch(url, {
    method: 'POST',
    headers: new Headers({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }),
    body: JSON.stringify(data)
  }).then(json => json.json())
};