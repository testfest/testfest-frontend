import React, { Fragment } from 'react';
import Header from "../../molecules/header/Header";
import Menu from "../../molecules/menu/Menu";
import AdminSpecialtiesContent from "../../organisms/admin-specialties-content/AdminSpecialtiesContent";

const AdminSpecialtiesPage = () => {
  return (
    <Fragment>
      <Header>
        <Menu/>
      </Header>
      <AdminSpecialtiesContent/>
    </Fragment>
  )
};

export default AdminSpecialtiesPage;
