import React, { Component } from 'react';
import { connect } from 'react-redux'

import { getTests, createTest } from './actions';
import { getCourses } from "../admin-specialties-content/actions";
import Entity from "../../molecules/entity/Entity";
import TableHeader from "../../atoms/table-header/TableHeader";
import TestForm from "../../molecules/test-form/TestForm";

import './AdminTestsContent.css';


class AdminTestsContent extends Component {
  componentDidMount() {
    const { getTests, getCourses } = this.props;
    getTests();
    getCourses();
  }

  render() {
    const { tests, courses, createTest } = this.props;
    const headers = [
      'Название теста',
      'Специализации',
      'Баллы',
      'Проходной балл',
      'Кол-во вопросов',
      'Время в минутах'
    ];

    const createSpecialtiesList = specialty => {
      return <li key={specialty}>{specialty}</li>
    };

    const createTestElement = test => {
      const { testId, testName, courses, points, minPoints, questionsCount, timeMinutes } = test;
      let testItems = [];
      testItems.push(testName);
      testItems.push(<ul
        className="admin-tests-content__specialties-list">
        {courses.map(createSpecialtiesList)}</ul>);
      testItems.push(points);
      testItems.push(minPoints);
      testItems.push(questionsCount);
      testItems.push(timeMinutes);
      return <Entity key={testId} entityItems={testItems} headers={headers}/>
    };
    const testElements = tests.map(test => createTestElement(test));

    return (
      <div className="admin-tests-content">
        <h1 className="admin-tests-content__heading">Тесты</h1>
        <TestForm courses={courses} onSubmit={createTest}/>
        <div className="admin-tests-list">
          <TableHeader headers={headers}/>
          {testElements}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  tests: state.adminTestsReducer.tests,
  courses: state.adminCoursesReducer.courses
});

const mapDispatchToProps = {
  getTests,
  getCourses,
  createTest
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminTestsContent);
