import React, { Component } from 'react';

import * as paths from '../../utils/paths';

import './style.css';


class ForbiddenPage extends Component {
  render() {
    return (
      <main className='forbidden-page'>
        <header className='forbidden-page__code'>403</header>
        <div className='forbidden-page__message'>У вас недостаточно прав доступа</div>
        <div>
          <a href={paths.LOGIN}>Вернуться на страницу входа</a>
        </div>
      </main>
    );
  }
}

export default ForbiddenPage;