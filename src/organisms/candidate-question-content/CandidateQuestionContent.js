import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from "react-redux";

import { getQuestions, submitResults, reset } from "./actions";
import Answer from "../../atoms/answer/Answer";
import Button from "../../atoms/button/Button";
import Sidebar from "../../molecules/sidebar/Sidebar";

import './CandidateQuestionContent.css';


class CandidateQuestionContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentQuestion: {
        answers: []
      },
      currentQuestionNumber: 1,
      time: {
        minutes: 90,
        seconds: 0
      },
      userAnswers: []
    };

    this.goToNextQuestion = this.goToNextQuestion.bind(this);
    this.goToPreviousQuestion = this.goToPreviousQuestion.bind(this);
    this.toggleAnswer = this.toggleAnswer.bind(this);
    this.selectQuestion = this.selectQuestion.bind(this);
    this.finishTest = this.finishTest.bind(this);
  }

  finishTest()  {
    const { testId, courseId, submitResults } = this.props;
    const { userAnswers } = this.state;
    submitResults(testId, userAnswers, courseId);
    clearInterval(this.timer);
  };

  selectQuestion(selectedQuestionNumber) {
    const { questions } = this.props;
    const selectedQuestion = questions[selectedQuestionNumber - 1];
    this.setState({
      currentQuestion: selectedQuestion,
      currentQuestionNumber: selectedQuestionNumber
    })
  }

  toggleAnswer(selectedAnswerId) {
    let { userAnswers } = this.state;
    const findSelectedAnswer = answer => {
      return answer.answerId === selectedAnswerId &&
        this.state.currentQuestion.questionId === answer.questionId;
    };
    const selectedAnswer = userAnswers.find(findSelectedAnswer);

    if (selectedAnswer) {
      userAnswers.splice(userAnswers.indexOf(selectedAnswer), 1);
    } else {
      userAnswers.push({
        answerId: selectedAnswerId,
        questionId: this.state.currentQuestion.questionId
      });
    }

    this.setState({
      userAnswers: userAnswers
    });
  }

  goToNextQuestion() {
    const questionsCount = this.props.questions.length;
    if (this.state.currentQuestionNumber === questionsCount) {
      return;
    }

    this.setState(prevState => {
      return {
        currentQuestion: this.props.questions[prevState.currentQuestionNumber],
        currentQuestionNumber: ++prevState.currentQuestionNumber
      }
    });
  }

  goToPreviousQuestion() {
    if (this.state.currentQuestionNumber === 1) {
      return;
    }

    this.setState(prevState => {
      return {
        currentQuestion: this.props.questions[prevState.currentQuestionNumber - 2],
        currentQuestionNumber: --prevState.currentQuestionNumber
      }
    });
  }

  componentDidMount() {
    const { getQuestions, testId } = this.props;
    getQuestions(testId);

    const decreaseTimeBySecond = time => {
      if (time.seconds === 0) {
        time.seconds = 59;
        time.minutes--;
      } else {
        time.seconds--;
      }
      return time;
    };

    this.timer = setInterval(() => {
      const time = decreaseTimeBySecond(this.state.time);
      if (time.minutes === 0 && time.seconds === 0) {
        this.finishTest();
      }
      this.setState({
        time: time
      })
    }, 1000)
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.success !== this.props.success) {
      const { test, questions } = this.props;
      const { timeMinutes } = test;

      this.setState({
        time: {
          minutes: timeMinutes,
          seconds: 0
        },
        currentQuestion: questions[0]
      })
    }
  }

  render() {
    const { test, questions } = this.props;
    const { title } = test;
    const { currentQuestionNumber, currentQuestion, time, userAnswers } = this.state;
    const { minutes, seconds } = time;

    const createAnswerElement = answer => {
      const hasSelectedAnswers = userAnswer => {
        return userAnswer.answerId === answer.id &&
          userAnswer.questionId === currentQuestion.questionId
      };
      const isSelected = userAnswers.some(hasSelectedAnswers);
      return <Answer
        key={answer.id}
        answer={{
          id: answer.id,
          text: answer.answer
        }}
        isSelected={isSelected}
        toggleAnswer={this.toggleAnswer}/>
    };
    const answerElements = currentQuestion.answers.map(createAnswerElement);

    let nextButtonText;
    let nextButtonClickHandler;
    if (currentQuestionNumber === questions.length) {
      nextButtonText = 'Завершить';
      nextButtonClickHandler = this.finishTest;
    } else {
      nextButtonText = 'Далее';
      nextButtonClickHandler = this.goToNextQuestion;
    }

    const addZeroBefore = number => {
      if (number < 10) {
        return `0${number}`;
      }
      return number;
    };

    const sidebarContent = [];
    for (let i = 1; i <= questions.length; i++) {
      sidebarContent.push({
        content: `Вопрос ${i}`,
        questionNumber: i,
        onSelect: this.selectQuestion.bind(this, i)
      });
    }
    sidebarContent.push({
      content: 'Завершить',
      onSelect: this.finishTest
    });

    return (
      <div className="candidate-question-content">
        <Sidebar items={sidebarContent} selectedQuestionNumber={currentQuestionNumber}/>
        <h1 className="candidate-question-content__title">{title}</h1>
        <p className="candidate-question-content__question-number">Вопрос {currentQuestionNumber}</p>
        <p>{currentQuestion.text}</p>
        <p className="candidate-question-content__time">Оставшееся время {minutes}:{addZeroBefore(seconds)}</p>
        <div className="candidate-question-content__answers">
          {answerElements}
        </div>
        <div className="candidate-question-content__buttons-container">
          <Button handleClick={this.goToPreviousQuestion} classNames={'button_size_middle button_center'}>Назад</Button>
          <Button handleClick={nextButtonClickHandler} classNames={'button_size_middle button_center'}>{nextButtonText}</Button>
        </div>
      </div>
    )
  }
}

CandidateQuestionContent.propTypes = {
  testId: PropTypes.string.isRequired,
  courseId: PropTypes.string.isRequired
};

const mapStateToProps = (state) => ({
  questions: state.questionsReducer.questions,
  success: state.questionsReducer.success,
  test: state.questionsReducer.test
});

const mapDispatchToProps = {
  getQuestions,
  submitResults,
  reset
};

export default connect(mapStateToProps, mapDispatchToProps)(CandidateQuestionContent);
