import { get, post, to } from "../../utils/apiServiceBase";
import history from "../../utils/history";
import * as paths from '../../utils/paths'

export const types = {
  GET_TEST_PENDING: 'GET_TEST_PENDING',
  GET_TEST_SUCCESS: 'GET_TEST_SUCCESS',
  GET_TEST_ERROR: 'GET_TEST_ERROR',
  GET_TEST_FLUSH: 'GET_TEST_FLUSH',
  SUBMIT_QUESTIONS_PENDING: 'SUBMIT_QUESTIONS_PENDING',
  SUBMIT_QUESTIONS_SUCCESS: 'SUBMIT_QUESTIONS_SUCCESS',
  SUBMIT_QUESTIONS_ERROR: 'SUBMIT_QUESTIONS_ERROR'
};

export const getTest = (testId) => {
  return async (dispatch) => {
    dispatch({
      type: types.GET_TEST_PENDING
    });

    const testUrl = `/api/tests/${testId}`;
    const [error, result] = await to(get(testUrl));

    if (error) {
      dispatch({
        type: types.GET_TEST_ERROR
      })
    }

    dispatch({
      type: types.GET_TEST_SUCCESS,
      test: result.data
    })
  }
};

export const submitQuestions = (testId, questions) => {
  return async (dispatch) => {
    dispatch({
      type: types.SUBMIT_QUESTIONS_PENDING
    });

    const url = `/api/questions`;
    const body = {
      testId: testId,
      questions: questions
    };
    const [error] = await to(post(url, body));

    if (error) {
      return dispatch({
        type: types.SUBMIT_QUESTIONS_ERROR
      });
    }

    dispatch({
      type: types.SUBMIT_QUESTIONS_SUCCESS
    });
    history.push(paths.ADMIN_TESTS)
  }
};

export const reset = () => {
  return (dispatch) => {
    dispatch({
      type: types.GET_TEST_FLUSH
    })
  }
};