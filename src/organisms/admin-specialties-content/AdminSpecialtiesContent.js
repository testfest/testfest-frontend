import React, { Component } from 'react';
import { connect } from 'react-redux'

import { getCourses, createCourse } from "./actions";
import TableHeader from "../../atoms/table-header/TableHeader";
import Entity from "../../molecules/entity/Entity";
import SpecialtyForm from "../../molecules/specialty-form/SpecialtyForm";

import './AdminSpecialtiesContent.css';


class AdminSpecialtiesContent extends Component {
  componentDidMount() {
    const { getCourses } = this.props;
    getCourses()
  }

  render() {
    const { createCourse, courses } = this.props;
    const headers = ['Название', 'Описание', 'Количество тестов'];

    const createSpecialtyElement = specialty => {
      const { id, courseName, courseDescription, testsCount } = specialty;
      const specialtyItems = [];
      specialtyItems.push(courseName);
      specialtyItems.push(courseDescription);
      specialtyItems.push(testsCount);
      return <Entity key={id} entityItems={specialtyItems} headers={headers}/>
    };
    const specialtyElements = courses.map(createSpecialtyElement);

    return(
      <div className="admin-specialties-content">
        <h1 className="admin-specialties-content__heading">Специализации</h1>
        <SpecialtyForm onSubmit={createCourse}/>
        <div className="admin-specialties-list">
          <TableHeader headers={headers}/>
          {specialtyElements}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  courses: state.adminCoursesReducer.courses
});

const mapDispatchToProps = {
  getCourses,
  createCourse
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminSpecialtiesContent);
