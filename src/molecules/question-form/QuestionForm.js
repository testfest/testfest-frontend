import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextInput from "../../atoms/text-input/TextInput";
import Select from "../../atoms/select/Select";
import Button from "../../atoms/button/Button";
import './QuestionForm.css';

class QuestionForm extends Component {
  constructor(props) {
    super(props);
    this.minimumAnswersCount = 3;
    this.maximumAnswersCount = 5;

    this.addAnswerElement = this.addAnswerElement.bind(this);
    this.removeAnswerElement = this.removeAnswerElement.bind(this);
  }

  addAnswerElement(event) {
    event.preventDefault();
    const { question, onAddNewAnswer } = this.props;
    if (question.answers.length === this.maximumAnswersCount) {
      alert(`В вопросе должно быть не больше ${this.maximumAnswersCount} ответов`);
      return;
    }
    onAddNewAnswer();
  }

  removeAnswerElement(event) {
    event.preventDefault();
    const { question, onRemoveLastQuestion} = this.props;
    if (question.answers.length === this.minimumAnswersCount) {
      alert(`В вопросе должно быть не меньше ${this.minimumAnswersCount} ответов`);
      return;
    }
    onRemoveLastQuestion();
  }

  render() {
    const { question, onChangeText, onChangePoints, onChangeAnswer } = this.props;
    let answerElements = [];
    for (let i = 0; i < question.answers.length; i++) {
      answerElements.push(
        <div key={i} className="question-form__answer">
          <div className="question-form__text-input-container">
            <TextInput
              handleChange={onChangeAnswer}
              type="text"
              name={`answer${i}`}
              classNames="text-input_size_full-width"
              value={question.answers[i].answer}
              placeholder="Текст ответа"/>
          </div>
          <div className="question-form__select-container">
            <Select
              classname="question-form__select"
              onChange={onChangeAnswer}
              name={`valid${i}`}
              selectedOption={question.answers[i].valid ? 'Верно' : 'Неверно'}
              options={['Неверно', 'Верно']}/>
          </div>
        </div>
      )
    }

    return (
      <form className="question-form">
        <TextInput
          handleChange={onChangeText}
          type="text"
          name="questionText"
          classNames="text-input_size_full-width"
          value={question && question.text ? question.text : ''}
          placeholder="Текст вопроса"/>
        <label className="question-form__number-input">
          <p className="question-form__label">Количество баллов</p>
          <TextInput
            handleChange={onChangePoints}
            type="number"
            name="score"
            value={question && question.points? question.points : 0}
            classNames="text-input_size_small"/>
        </label>
        {answerElements}
        <div className='question-form__footer'>
          <Button handleClick={this.addAnswerElement} classNames="button_size_middle button_center">Добавить ответ</Button>
          <Button handleClick={this.removeAnswerElement} classNames="button_size_middle button_center">Убрать ответ</Button>
        </div>
      </form>
    )
  }
}

QuestionForm.propTypes = {
  question: PropTypes.shape({
    id: PropTypes.number.isRequired,
    text: PropTypes.string.isRequired,
    points: PropTypes.number.isRequired,
    answers: PropTypes.arrayOf(PropTypes.shape({
      answer: PropTypes.string.isRequired,
      valid: PropTypes.bool.isRequired
    }).isRequired,).isRequired
  }),
  onChangeText: PropTypes.func.isRequired,
  onChangePoints: PropTypes.func.isRequired,
  onChangeAnswer: PropTypes.func.isRequired,
  onAddNewAnswer: PropTypes.func.isRequired,
  onRemoveLastQuestion: PropTypes.func.isRequired
};

export default QuestionForm;
