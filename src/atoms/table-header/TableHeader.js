import React from 'react';
import PropTypes from 'prop-types';
import './TableHeader.css';

const TableHeader = ({ headers }) => {
  const createHeaderCells = item => {
    return <div key={item} className="table-header__cell">{item}</div>
  };
  const headerCells = headers.map(createHeaderCells);

  return(
    <div className="table-header">
      {headerCells}
    </div>
  )
};

TableHeader.propTypes = {
  headers: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired
};

export default TableHeader;
