import React, { Fragment, Component } from 'react';
import { withRouter } from "react-router-dom";

import CandidateTestsContent from "../../organisms/candidate-tests-content/CandidateTestsContent";
import UserHeader from "../../organisms/user-header/UserHeader";

class CandidateTestsPage extends Component {
  render() {
    const { params } = this.props.match;
    const { course } = params;

    return (
      <Fragment>
        <UserHeader/>
        <CandidateTestsContent courseId={course}/>
      </Fragment>
    )
  }
}

export default withRouter(CandidateTestsPage);
