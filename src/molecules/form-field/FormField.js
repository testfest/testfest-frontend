import React from 'react';
import PropTypes from 'prop-types';

import './style.css';


const FormField = (props) => {
  const { children, title } = props;

  return (
    <div>
      <div className='form-field__label'>{title}</div>
      {children}
    </div>
  )
};

FormField.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string
};

FormField.defaultProps = {
  title: ''
};

export default FormField;