import React from 'react';
import PropTypes from "prop-types";
import clsx from "clsx";
import './EntityItem.css';

const EntityItem = ({ description, children }) => {
  const childrenShouldBeCenter = typeof children === "number" ||
    (typeof children == "string" && children.length <= 7 && /\d/.test(children));

  const classList = clsx('entity-item',
    childrenShouldBeCenter && 'entity-item_text-align_center',
    !childrenShouldBeCenter && 'entity-item_text-align_left');

  return (
    <div className={classList}>
      <b className="entity-item__description">{description}: </b>
      {children}
    </div>
  )
};

EntityItem.propTypes = {
  description: PropTypes.string.isRequired,
};

export default EntityItem;
