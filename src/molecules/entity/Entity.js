import React from 'react';
import PropTypes from 'prop-types';
import EntityItem from "../../atoms/entity-item/EntityItem";
import './Entity.css';

const Entity = ({ entityItems, headers }) => {
  let entityItemElements = [];
  for (let i = 0; i < headers.length; i++) {
    entityItemElements.push(
      <EntityItem key={i} description={headers[i]}>
        {entityItems[i]}
      </EntityItem>
    );
  }

  return (
    <div className="entity">
      {entityItemElements}
    </div>
  )
};

Entity.propTypes = {
  entityItems: PropTypes.arrayOf(PropTypes.any.isRequired).isRequired,
  headers: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired
};

export default Entity;
